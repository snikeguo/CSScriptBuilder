//css_import ArmNoneEabiGccCppModule
using System;
using System.Threading;
using Build;
using CppBuild;
namespace testDll
{
    public class host
    {
        public static void Main(string[] d)
        {
            ArmNoneEabiGccCppModule armncc = new ArmNoneEabiGccCppModule();
            armncc.IsDebugBuilderLibrary = true;            
            armncc.AddCIncludePath(@"F:\VSProject\RTTVS2\RTTVS2\Drivers\CMSIS\Device\ST\STM32F1xx\Include",
                @"F:\VSProject\RTTVS2\RTTVS2\Drivers\CMSIS\Include",
                @"F:\VSProject\RTTVS2\RTTVS2\Drivers\STM32F1xx_HAL_Driver\Inc",
                @"F:\VSProject\RTTVS2\RTTVS2\Drivers\STM32F1xx_HAL_Driver\Inc\Legacy",
                @"F:\VSProject\RTTVS2\RTTVS2\Inc",
                @"F:\VSProject\RTTVS2\RTTVS2\RTThread\Kernel\include",
                @"F:\VSProject\RTTVS2\RTTVS2\RTThread\Port\inc");
            armncc.AddCDefineList("STM32F103xE", "ARM_MATH_CM3", "STM32F103ZE");
            armncc.AddCFile("main.c");
            armncc.AddCFile("gpio.c");
            armncc.AddCFile("mainTemplate.c");
            armncc.AddCFile("RtThreadInit.c");
            armncc.AddCFile("stm32f1xx_hal_msp.c");
            armncc.AddCFile("stm32f1xx_it.c");
            armncc.AddCFile("usart.c");
            armncc.AddCFile("startup_stm32f103xg.s");
            armncc.ArmThumb = ArmNoneEabiGccCppModule.ArmThumbs.Thumb;
            armncc.ToolsPath = "E:/gcc-arm-none-eabi-4_9_win32";
            armncc.Chip = ArmNoneEabiGccCppModule.Chips.Cortex_M3;
            armncc.CStandard = Build.CppBuild.CStandardTypes.GNUC99;
            armncc.OutBuildPath = "F:/VSProject/RTTVS2/RTTVS2/OutPut";
            armncc.Building();

        }
    }
}