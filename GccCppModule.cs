//css_import Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Build.CppBuild;
namespace CppBuild
{
    public class ArmNoneEabiGccCppModule:CppModule
    {
        public enum Chips
        {
            Cortex_M0,
            Cortex_M0Plus,
            Cortex_M4,
            Cortex_M7,
        };
        public enum ArmThumbs
        {
            Arm,
            Thumb,
        };
        public enum FpuTypes
        {
            NoFpu,
            SoftFpu,
            HardFpu,
        }
        public override ComplierTypes ComplierType
        {
            get
            {
                return ComplierTypes.GCC;    
            }
        }
        public override string Description
        {
            get
            {
                return "此编译器用来编译ARM Cortex-M ,Cortex-R 的芯片。";
            }
        }
        public Chips Chip { get; set; }
        public ArmThumbs ArmThumb { get; set; }
        public FpuTypes FpuType { get; set; }

        public override string Version
        {
            get
            {
                return "0.0.1";
            }
        }


        public ArmNoneEabiGccCppModule()
        {
            Console.WriteLine(this.Description);
            Console.WriteLine("作者：蒙塔基的钢蛋儿，E-Mail:snikeguo@foxmail.com  408260925@qq.com");
            Console.WriteLine(this.Version);
        }
        public override void Building()
        {
            throw new NotImplementedException();
        }

        public override void Clean()
        {
            throw new NotImplementedException();
        }
    }
}
